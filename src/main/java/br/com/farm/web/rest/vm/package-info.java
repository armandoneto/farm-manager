/**
 * View Models used by Spring MVC REST controllers.
 */
package br.com.farm.web.rest.vm;
